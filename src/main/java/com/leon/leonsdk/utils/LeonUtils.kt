package com.leon.leonsdk.utils

import android.util.Log

object LeonUtils {

    fun showLog(msg: String?) {
        msg?.let {
            Log.i("TAG_LEON", "输出日志: $msg")
        }
    }
}